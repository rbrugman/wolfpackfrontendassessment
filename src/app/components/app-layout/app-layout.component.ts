import { Component, OnInit } from '@angular/core';

/**
 * Component which functions as the base layout of the application.
 */
@Component({
  selector: 'app-layout',
  templateUrl: './app-layout.component.html',
  styleUrls: ['./app-layout.component.scss']
})
export class AppLayoutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
