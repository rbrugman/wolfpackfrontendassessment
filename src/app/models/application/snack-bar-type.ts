/**
 * Enum for defining the types of snackbar the application supports.
 */
export enum SnackBarType {
    info,
    error,
    success,
}
