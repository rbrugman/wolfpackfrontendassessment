export interface PackResource {
    name: string;
    lat: string;
    lng: string;
}
