export interface WolfResource {
    name: string;
    gender: string;
    birthday: string;
}
