export interface Wolf {
    id: number;
    name: string;
    gender: string;
    birthday: string;
    created_at: Date;
    updated_at: Date;
}
