import { Wolf } from './wolf';

export interface Pack {
    id: number;
    name: string;
    lat: string;
    lng: string;
    created_at: Date;
    updated_at: Date;
    wolves: Wolf[];
}
