/**
 * Default form error messages.
 */
export class FormErrorMessages {
    public static readonly required = 'This field is required';
}
