/**
 * Supported API routes & sub-routes.
 */
export class ApiRoutes {
    public static wolvesRoute = 'wolves';
    public static packsRoute = 'packs';

    public static wolfSubRoute = 'wolf';
}
