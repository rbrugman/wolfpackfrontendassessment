import { WolfResource } from './../models/resources/wolf-resource';
import { Injectable } from '@angular/core';
import { HttpBaseService } from './http-base-service';
import { Wolf } from '../models/domain/wolf';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiRoutes } from '../config/api-config';
import { Observable } from 'rxjs';

/**
 * Service to communicate with the wolves endpoint.
 */
@Injectable({
  providedIn: 'root'
})
export class WolvesService extends HttpBaseService<Wolf, WolfResource, WolfResource> {

  constructor(httpClient: HttpClient) {
    super(
      httpClient,
      `${environment.apiBaseUrl}/${ApiRoutes.wolvesRoute}`
    );
  }

}
