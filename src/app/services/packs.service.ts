import { PackResource } from 'src/app/models/resources/pack-resource';
import { ApiRoutes } from './../config/api-config';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpBaseService } from './http-base-service';
import { HttpClient } from '@angular/common/http';
import { Pack } from '../models/domain/pack';
import { Observable } from 'rxjs';

/**
 * Service to communicate with packs endpoint.
 */
@Injectable({
  providedIn: 'root'
})
export class PacksService extends HttpBaseService<Pack, PackResource, PackResource> {

  constructor(httpClient: HttpClient) {
    super(
      httpClient,
      `${environment.apiBaseUrl}/${ApiRoutes.packsRoute}`
    );
  }

  public postWolf(id: number, wolf_id: number): Observable<{}> {
    return this.http.post<{}>(`${this.url}/${id}/${ApiRoutes.wolfSubRoute}/${wolf_id}`, {});
  }

  public deleteWolf(id: number, wolf_id: number): Observable<{}> {
    return this.http.delete<{}>(`${this.url}/${id}/${ApiRoutes.wolfSubRoute}/${wolf_id}`, {});
  }

}
