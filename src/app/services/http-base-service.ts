import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

/**
 * Base Service which other services can implement to gain all standard API CRUD functionality.
 */
export abstract class HttpBaseService<T, TPost, TUpdate> {
  constructor(
    protected http: HttpClient,
    protected readonly url: string) { }

  public getAll(): Observable<T[]> {
    return this.http.get<T[]>(`${this.url}`);
  }

  public get(id: number): Observable<T> {
    return this.http.get<T>(`${this.url}/${id}`);
  }

  public post(resource: TPost): Observable<{}> {
    return this.http.post<{}>(`${this.url}`, resource);
  }

  public put(resource: TUpdate, resourceId: number): Observable<{}> {
    return this.http.put<{}>(`${this.url}/${resourceId}`, resource);
  }

  public delete(id: number): Observable<{}> {
    return this.http.delete(`${this.url}/${id}`);
  }
}
