import { ButtonLabels } from './../config/button-labels';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackBarType } from '../models/application/snack-bar-type';

/**
 * Mapper to go from a SnackBarType to a css class.
 */
const cssClassMapper = {
  info: 'snackbar-info',
  error: 'snackbar-error',
  success: 'snackbar-success',
};


/**
 * Service for opening snackbars.
 */
@Injectable({
  providedIn: 'root'
})
export class SnackbarService {

  constructor(public snackBar: MatSnackBar) { }
  /**
   * Method to open a error snackbar.
   * @param message Message to display.
   * @param type The SnackBarType of the event.
   * @param action (Optional) The text of the action button. If empty no default will be displayed.
   */
  public openSnackBarSuccess(message: string, action?: string): void {
    this.openSnackBar(message, SnackBarType.success, action);
  }

  /**
   * Method to open a error snackbar.
   * @param message Message to display.
   * @param type The SnackBarType of the event.
   * @param action (Optional) The text of the action button. If empty no default will be displayed.
   */
  public openSnackBarError(message: string, action?: string): void {
    this.openSnackBar(message, SnackBarType.error, action);
  }

  /**
   * Method to open a info snackbar.
   * @param message Message to display.
   * @param type The SnackBarType of the event.
   * @param action (Optional) The text of the action button. If empty no default will be displayed.
   */
  public openSnackBarInfo(message: string, action?: string): void {
    this.openSnackBar(message, SnackBarType.info, action);
  }

  /**
   * Method to open a snack bar of a certain snackbar type.
   */
  private openSnackBar(message: string, type: SnackBarType, action?: string) {
    const cssClass: string = cssClassMapper[SnackBarType[type]];
    if (!cssClass) {
      return;
    }

    let snackbarAction: string = ButtonLabels.close;
    if (action) {
      snackbarAction = action;
    }

    this.snackBar.open(message, snackbarAction, {
      duration: 1500,
      panelClass: [cssClass]
    });
  }

}
