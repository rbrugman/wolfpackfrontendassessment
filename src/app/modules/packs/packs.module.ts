import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PacksRoutingModule } from './packs-routing.module';
import { PacksOverviewComponent } from './components/packs-overview/packs-overview.component';
import { PacksTableComponent } from './components/packs-table/packs-table.component';
import { PacksGroupsComponent } from './components/packs-groups/packs-groups.component';
import { PacksMapComponent } from './components/packs-map/packs-map.component';
import { PacksAddEditComponent } from './components/packs-add-edit/packs-add-edit.component';

@NgModule({
  declarations: [
    PacksOverviewComponent,
    PacksTableComponent,
    PacksGroupsComponent,
    PacksMapComponent,
    PacksAddEditComponent],
  imports: [
    CommonModule,
    PacksRoutingModule,
    SharedModule,
    LeafletModule
  ]
})
export class PacksModule { }
