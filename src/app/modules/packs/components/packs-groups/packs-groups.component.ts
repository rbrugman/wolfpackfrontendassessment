import { SnackbarService } from 'src/app/services/snackbar.service';
import { WolvesService } from 'src/app/services/wolves.service';
import { PacksService } from 'src/app/services/packs.service';
import { Pack } from 'src/app/models/domain/pack';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormControl } from '@angular/forms';
import { Wolf } from 'src/app/models/domain/wolf';

/**
 * Component for adding and removing wolves from a pack.
 */
@Component({
  selector: 'app-packs-groups',
  templateUrl: './packs-groups.component.html',
  styleUrls: ['./packs-groups.component.scss']
})
export class PacksGroupsComponent implements OnInit, OnDestroy {

  @Input() packs: Pack[];

  /**
   * The selected pack.
   */
  public selectedPack: Pack = null;

  /**
   * FormControl and ValueChanges subscription for selectedPack.
   */
  public selectFormControl: FormControl;
  public selectFormControlSubscription: Subscription;

  /**
   * Variable to control the displaying of the add wolf modal.
   */
  public displayAddWolfModal = false;

  /**
   * FormControl and ValueChanges subscription for selectedWolf.
   */
  public selectedWolf: Wolf = null;
  public selectWolfFormControl: FormControl;
  public selectWolfFormControlSubscription: Subscription;

  /**
   * Lists containing all the wolves of the application and the available wolves
   * (these are wolves which not already related to the selected pack).
   */
  public allWolves: Wolf[];
  public availableWolves: Wolf[];

  constructor(
    private packsService: PacksService,
    private wolvesService: WolvesService,
    private snackbarService: SnackbarService
  ) {
    this.selectFormControl = new FormControl('');
    this.selectWolfFormControl = new FormControl('');
  }

  ngOnInit() {
    /**
     * Value subscription for the pack selector.
     * Fetches the pack details.
     */
    this.selectFormControlSubscription = this.selectFormControl.valueChanges.subscribe((selectedId: number) => {
      const foundPack = this.packs.find(pack => pack.id === selectedId);
      if (foundPack == null) {
        return;
      }
      this.fetchPackDetail(selectedId);
    });

    /**
     * Value subscription for the pack selector.
     * Fetches the pack details.
     */
    this.selectWolfFormControlSubscription = this.selectWolfFormControl.valueChanges.subscribe((selectedId: number) => {
      this.selectedWolf = this.availableWolves.find(wolf => wolf.id === selectedId);
    });

    /**
     * Fetch all the wolves of application for adding wolves to a pack.
     */
    this.wolvesService.getAll().subscribe(allWolves => {
      this.allWolves = allWolves;
    });
  }

  ngOnDestroy() {
    // Clean up of subscription when the component destroys.
    this.selectFormControlSubscription.unsubscribe();
    this.selectWolfFormControlSubscription.unsubscribe();
  }

  /**
   * Method which triggers when the add wolf button is pressed on a pack card.
   * Fetches the available wolves and displays the add wolf modal.
   */
  public onClickAddWolf(): void {
    if (this.allWolves == null) {
      this.snackbarService.openSnackBarError('There are no available wolves to add to the pack');
      return;
    }
    this.filterAvailableWolves();
    this.displayAddWolfModal = true;
  }

  /**
   * Method which triggers when the add wolf submit button is pressed in the add wolf modal.
   * Adds the wolf to the pack.
   */
  public onClickSubmitAddWolf(): void {
    this.selectedPack.wolves.push(this.selectedWolf);

    this.packsService.postWolf(this.selectedPack.id, this.selectedWolf.id).subscribe(() => {
      this.displayAddWolfModal = false;
      this.snackbarService.openSnackBarSuccess('Wolf was successfully added to pack');
    }, () => {
      // Put the wolf back in the array in case something went wrong.
      const index = this.selectedPack.wolves.findIndex(wolf => wolf.id === this.selectedWolf.id);
      this.selectedPack.wolves.splice(index, 1);
      this.snackbarService.openSnackBarError('Wolf could not be added to pack');
    });
  }

  /**
   * Method which triggers when the delete wolf button is pressed.
   * Removes the wolf from the pack.
   * @param wolf the wolf to delete.
   */
  public onClickDeleteWolf(wolf: Wolf) {
    if (wolf.id <= 0 || this.selectedPack.id <= 0) {
      this.snackbarService.openSnackBarError('Wolf could not be deleted');
    }

    const index = this.selectedPack.wolves.findIndex(selectedPackWolf => selectedPackWolf.id === wolf.id);
    this.selectedPack.wolves.splice(index, 1);

    this.packsService.deleteWolf(this.selectedPack.id, wolf.id).subscribe(() => {
      this.snackbarService.openSnackBarSuccess('Wolf was successfully removed from pack');
    }, () => {
      // Remove the wolf from the array in case something went wrong.
      this.selectedPack.wolves.push(this.selectedWolf);
      this.snackbarService.openSnackBarError('Wolf could not be deleted');
    });
  }

  /**
   * Method to fetch the details of a pack.
   * @param id the id of the pack to fetch the details for.
   */
  private fetchPackDetail(id: number): void {
    this.availableWolves = null;
    this.packsService.get(id).subscribe(result => {
      this.selectedPack = result;
      this.filterAvailableWolves();
    }, () => {
      this.snackbarService.openSnackBarError('Pack details could not be fetched');
    });
  }

  /**
   * Method to filter out wolves which are related to the selected pack.
   * Stores the list of available wolves.
   */
  private filterAvailableWolves(): void {
    if (this.allWolves == null) {
      this.snackbarService.openSnackBarError('Available wolves could not be determined');
      return;
    }
    this.availableWolves = this.allWolves.filter(wolf => {
      return this.selectedPack.wolves.findIndex(unavailableWolf => unavailableWolf.id === wolf.id) === -1;
    });
  }
}
