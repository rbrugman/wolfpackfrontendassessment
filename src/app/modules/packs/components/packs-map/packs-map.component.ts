import { Component, Input, OnChanges } from '@angular/core';
import { Pack } from 'src/app/models/domain/pack';
import { latLng, marker, tileLayer, icon } from 'leaflet';

/**
 * Component to display the packs and their location on a map.
 */
@Component({
  selector: 'app-packs-map',
  templateUrl: './packs-map.component.html',
  styleUrls: ['./packs-map.component.scss']
})
export class PacksMapComponent implements OnChanges {

  /**
   * Input variable containing all the packs to display.
   */
  @Input() packs: Pack[];

  /**
   * Variables for configuring the Leaflet map.
   * For more info on Leaflet: https://github.com/Asymmetrik/ngx-leaflet
   */
  public options = {
    layers: [tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { attribution: 'Open Street Map' })],
    zoom: 7,
    center: latLng([51.449398, 5.494966])
  };
  public layers = [];

  constructor() { }

  ngOnChanges(): void {
    this.configureMap();
  }

  /**
   * Method to configure the map with the pack markers.
   */
  private configureMap(): void {
    if (this.packs == null || this.packs.length <= 0) {
      return;
    }
    // Clear the layers array
    this.layers.length = 0;
    this.packs.forEach(pack => {
      if (pack == null) {
        return;
      }
      // Create a marker for each pack.
      const newMarker = marker(
        [Number.parseInt(pack.lat, 10), Number.parseInt(pack.lng, 10)],
        {
          title: pack.name,
          // The icon needs to be explicitly because of the way the webpack bundler works.
          // https://github.com/Asymmetrik/ngx-leaflet#a-note-about-markers
          icon: icon({
            iconSize: [25, 41],
            iconAnchor: [13, 41],
            iconUrl: 'assets/marker-icon.png',
            shadowUrl: 'assets/marker-shadow.png'
          })
        }
      );
      this.layers.push(newMarker);
    });

  }


}
