import { PacksService } from 'src/app/services/packs.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Pack } from 'src/app/models/domain/pack';
import { Router, ActivatedRoute } from '@angular/router';
import { SnackbarService } from 'src/app/services/snackbar.service';
import { FormErrorMessages } from 'src/app/config/form-error-messages';
import { PackResource } from 'src/app/models/resources/pack-resource';

/**
 * Component to add and edit packs.
 */
@Component({
  selector: 'app-packs-add-edit',
  templateUrl: './packs-add-edit.component.html',
  styleUrls: ['./packs-add-edit.component.scss']
})
export class PacksAddEditComponent implements OnInit {

  public componentTitle = 'Add Pack';

  public packForm: FormGroup;
  public selectedBirthday: Date;

  public packToEdit: Pack;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private packsService: PacksService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    // Build the form group.
    this.packForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('^[a-zA-Z]+$')]],
      latitude: ['', [Validators.required, Validators.min(-90), Validators.max(90)]],
      longitude: ['', [Validators.required, Validators.min(-180), Validators.max(180)]],
    });

    // Fetch the route id and if present configure the component for editing.
    const routeId = Number.parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    if (routeId > 0) {
      this.componentTitle = 'Edit Pack';

      this.packsService.get(routeId).subscribe(result => {
        this.packToEdit = result;
        this.setPackAsFormValue(result);
      }, () => {
        this.snackbarService.openSnackBarError('Could not fetch pack');
      });
    }
  }

  /**
   * Getter to grant template access to form error messages.
   */
  get formErrorMessages() {
    return FormErrorMessages;
  }

  /**
   * Method which triggers when a user clicks the back to overview button.
   * Redirects the user to the packs overview.
   */
  public onClickBackToOverview(): void {
    this.router.navigate(['packs/overview']);
  }

  /**
   * Method which triggers when a user clicks the submit button.
   * Creates or updates the pack.
   */
  public onClickSubmit(): void {
    if (!this.packForm.valid) {
      return;
    }

    if (this.packToEdit == null) {
      this.createPack();
    } else {
      this.updatePack();
    }
  }

  /**
   * Method which triggers when the birthday datepicker changes.
   * @param newDate the new date.
   */
  public onBirthdayChanged(newDate: Date): void {
    this.selectedBirthday = newDate;
  }

  /**
   * Method to set the values of a pack in the form group. Used for editing packs.
   * @param pack The pack to set the values of the form for.
   */
  private setPackAsFormValue(pack: Pack) {
    this.packForm.get('name').setValue(pack.name);
    this.packForm.get('latitude').setValue(pack.lat);
    this.packForm.get('longitude').setValue(pack.lng);
  }

  /**
   * Method to create a pack.
   */
  private createPack(): void {
    const pack: PackResource = this.createResource();
    this.packsService.post(pack).subscribe(() => {
      this.snackbarService.openSnackBarSuccess('Creation of pack was successful');
      this.router.navigate(['packs/overview']);
    }, () => {
      this.snackbarService.openSnackBarError('Creation of pack was not successful');
    });
  }

  /**
   * Method to update a pack.
   */
  private updatePack(): void {
    const pack: PackResource = this.createResource();
    this.packsService.put(pack, this.packToEdit.id).subscribe(() => {
      this.snackbarService.openSnackBarSuccess('Updating of pack was successful');
      this.router.navigate(['packs/overview']);
    }, () => {
      this.snackbarService.openSnackBarError('Updating of pack was not successful');
    });
  }

  /**
   * Method to construct the resource.
   */
  private createResource(): PackResource {
    const packResource: PackResource = {
      name: this.packForm.get('name').value,
      lat: this.packForm.get('latitude').value.toString(),
      lng: this.packForm.get('longitude').value.toString(),
    };
    return packResource;
  }

}
