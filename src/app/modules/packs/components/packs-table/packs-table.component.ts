import { SnackbarService } from 'src/app/services/snackbar.service';
import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { Pack } from 'src/app/models/domain/pack';
import { PacksService } from 'src/app/services/packs.service';

/**
 * Component to display the packs of the application in a datatable.
 */
@Component({
  selector: 'app-packs-table',
  templateUrl: './packs-table.component.html',
  styleUrls: ['./packs-table.component.scss']
})
export class PacksTableComponent implements OnInit {

  @Input() packs: Pack[];

  constructor(
    private router: Router,
    private packsService: PacksService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
  }

  /**
   * Method which triggers when a user clicks the edit pack button.
   * Redirects the user to the edit pack component.
   * @param pack the pack to edit.
   */
  public onClickEditPack(pack: Pack): void {
    if (pack == null && pack.id < 0) {
      this.snackbarService.openSnackBarError('Could not find the pack to edit');
      return;
    }
    this.router.navigate([`packs/edit/${pack.id}`]);
  }

  /**
   * Method which triggers when a user clicks the delete pack button.
   * Removes the pack.
   * @param clickedPack the pack to remove.
   */
  public onClickDeletePack(clickedPack: Pack): void {
    const clickedPackIndex = this.packs.findIndex(pack => pack.id === clickedPack.id);

    this.packs.splice(clickedPackIndex, 1);
    this.packsService.delete(clickedPack.id).subscribe(() => {
      this.snackbarService.openSnackBarError('Pack was successfully removed');

    }, () => {
      this.packs.splice(clickedPackIndex, 0, clickedPack);
      this.snackbarService.openSnackBarError('Could not remove the pack');
    });
  }

}
