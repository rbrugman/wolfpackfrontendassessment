import { SnackbarService } from 'src/app/services/snackbar.service';
import { finalize } from 'rxjs/operators';
import { PacksService } from 'src/app/services/packs.service';
import { Pack } from 'src/app/models/domain/pack';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

/**
 * Component which functions as the overview for the packs module.
 */
@Component({
  selector: 'app-packs-overview',
  templateUrl: './packs-overview.component.html',
  styleUrls: ['./packs-overview.component.scss']
})
export class PacksOverviewComponent implements OnInit {

  public retrievedPacks: Pack[];
  public loadingPacks = true;

  constructor(
    private packsService: PacksService,
    private router: Router,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    this.packsService.getAll()
      .pipe(
        finalize(() => this.loadingPacks = false)
      )
      .subscribe(results => {
        this.retrievedPacks = results;
      }, () => {
        this.snackbarService.openSnackBarError('Could not fetch the wolves');
      });
  }

  public onClickAddPack(): void {
    this.router.navigate(['packs/add']);
  }

}
