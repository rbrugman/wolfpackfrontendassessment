import { PacksAddEditComponent } from './components/packs-add-edit/packs-add-edit.component';
import { PacksOverviewComponent } from './components/packs-overview/packs-overview.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'overview' },
  { path: 'overview', component: PacksOverviewComponent },
  { path: 'add', component: PacksAddEditComponent },
  { path: 'edit/:id', component: PacksAddEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PacksRoutingModule { }
