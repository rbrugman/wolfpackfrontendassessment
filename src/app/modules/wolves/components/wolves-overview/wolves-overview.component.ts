import { SnackbarService } from 'src/app/services/snackbar.service';
import { WolvesService } from 'src/app/services/wolves.service';
import { Component, OnInit } from '@angular/core';
import { Wolf } from 'src/app/models/domain/wolf';
import { Router } from '@angular/router';
import { finalize } from 'rxjs/operators';

/**
 * Component to display a overview of wolves.
 */
@Component({
  selector: 'app-wolves-overview',
  templateUrl: './wolves-overview.component.html',
  styleUrls: ['./wolves-overview.component.scss']
})
export class WolvesOverviewComponent implements OnInit {

  public retrievedWolves: Wolf[];
  public loadingWolves = true;

  constructor(
    private wolvesService: WolvesService,
    private router: Router,
    private snackbarService: SnackbarService) { }

  ngOnInit() {
    this.wolvesService
      .getAll()
      .pipe(
        finalize(() => this.loadingWolves = false)
      )
      .subscribe(results => {
        this.retrievedWolves = results;
      }, error => {
        this.snackbarService.openSnackBarError('Could not fetch wolves');
      });
  }

  /**
   * Method which triggers when a user clicks the add wolf button.
   * Redirects the user to the add wolf component.
   */
  public onClickAddWolf(): void {
    this.router.navigate(['wolves/add']);
  }

}
