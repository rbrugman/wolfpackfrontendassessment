import { SnackbarService } from 'src/app/services/snackbar.service';

import { WolvesService } from 'src/app/services/wolves.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Wolf } from 'src/app/models/domain/wolf';
import { DateUtils } from 'src/app/utils/date-utils';
import { FormErrorMessages } from 'src/app/config/form-error-messages';

@Component({
  selector: 'app-wolves-add-edit',
  templateUrl: './wolves-add-edit.component.html',
  styleUrls: ['./wolves-add-edit.component.scss']
})
export class WolvesAddEditComponent implements OnInit {

  public componentTitle = 'Add Wolf';

  public wolfForm: FormGroup;
  public selectedBirthday: Date;

  public wolfToEdit: Wolf;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private wolvesService: WolvesService,
    private snackbarService: SnackbarService
  ) { }

  ngOnInit() {
    // Build the form group.
    this.wolfForm = this.formBuilder.group({
      name: ['', Validators.required],
      gender: ['male'],
      birthday: [null, Validators.required]
    });

    // Fetch the route id and if present configure the component for editing.
    const routeId = Number.parseInt(this.activatedRoute.snapshot.paramMap.get('id'), 10);
    if (routeId > 0) {
      this.componentTitle = 'Edit Wolf';

      this.wolvesService.get(routeId).subscribe(result => {
        this.wolfToEdit = result;
        this.setWolfAsFormValue(result);
      }, () => {
        this.snackbarService.openSnackBarError('Could not fetch wolf');
      });
    }
  }

  /**
   * Getter to grant template access to form error messages.
   */
  get formErrorMessages() {
    return FormErrorMessages;
  }

  /**
   * Method which triggers when a user clicks the back to overview button.
   * Redirects the user to the wolves overview.
   */
  public onClickBackToOverview(): void {
    this.router.navigate(['wolves/overview']);
  }

  /**
   * Method which triggers when a user clicks the submit button.
   * Creates or updates the wolf.
   */
  public onClickSubmit(): void {
    if (!this.wolfForm.valid) {
      return;
    }

    if (this.wolfToEdit == null) {
      this.createWolf();
    } else {
      this.updateWolf();
    }
  }

  /**
   * Method which triggers when the birthday datepicker changes.
   * @param newDate the new date.
   */
  public onBirthdayChanged(newDate: Date): void {
    this.selectedBirthday = newDate;
  }

  /**
   * Method to set the values of a wolf in the form group. Used for editing wolves.
   * @param wolf The wolf to set the values of the form for.
   */
  private setWolfAsFormValue(wolf: Wolf) {
    this.wolfForm.get('name').setValue(wolf.name);
    this.wolfForm.get('birthday').setValue(DateUtils.reverseDateString(wolf.birthday));
    this.wolfForm.get('gender').setValue(wolf.gender);

    // this.selectedBirthday = new Date(DateUtils.reverseDateString(wolf.birthday));
  }

  /**
   * Method to create a wolf.
   */
  private createWolf(): void {
    const wolf: Wolf = this.wolfForm.value;
    wolf.birthday = DateUtils.formatDateToDateString(this.selectedBirthday);

    this.wolvesService.post(wolf).subscribe(() => {
      this.snackbarService.openSnackBarSuccess('Creation of wolf was successful');
      this.router.navigate(['wolves/overview']);
    }, () => {
      this.snackbarService.openSnackBarError('Creation of wolf was not successful');
    });
  }

  /**
   * Method to update a wolf.
   */
  private updateWolf(): void {
    const wolf: Wolf = this.wolfForm.value;

    // Override birthday if the date picker value has changed.
    if (this.selectedBirthday) {
      wolf.birthday = DateUtils.formatDateToDateString(this.selectedBirthday);
    } else {
      wolf.birthday = DateUtils.reverseDateString(wolf.birthday);
    }

    this.wolvesService.put(wolf, this.wolfToEdit.id).subscribe(() => {
      this.snackbarService.openSnackBarSuccess('Updating of wolf was successful');
      this.router.navigate(['wolves/overview']);
    }, () => {
      this.snackbarService.openSnackBarError('Updating of wolf was not successful');
    });
  }

}
