import { WolvesService } from 'src/app/services/wolves.service';
import { Router } from '@angular/router';
import { Component, Input } from '@angular/core';
import { Wolf } from 'src/app/models/domain/wolf';
import { SnackbarService } from 'src/app/services/snackbar.service';

@Component({
  selector: 'app-wolves-table',
  templateUrl: './wolves-table.component.html',
  styleUrls: ['./wolves-table.component.scss']
})
export class WolvesTableComponent {

  @Input() wolves: Wolf[];

  constructor(
    private router: Router,
    private wolvesService: WolvesService,
    private snackbarService: SnackbarService
  ) { }

  /**
   * Method which triggers when a user clicks the edit wolf button.
   * Redirects the user to the edit wolf component.
   * @param wolf the wolf to edit.
   */
  public onClickEditWolf(wolf: Wolf): void {
    if (wolf == null && wolf.id < 0) {
      this.snackbarService.openSnackBarError('Could not fetch wolf to edit');
      return;
    }
    this.router.navigate([`wolves/edit/${wolf.id}`]);
  }

  /**
   * Method which triggers when a user clicks the delete wolf button.
   * Removes the wolf.
   * @param clickedWolf the wolf to remove.
   */
  public onClickDeleteWolf(clickedWolf: Wolf): void {
    const clickedWolfIndex = this.wolves.findIndex(wolf => wolf.id === clickedWolf.id);

    this.wolves.splice(clickedWolfIndex, 1);
    this.wolvesService.delete(clickedWolf.id).subscribe(() => {
      this.snackbarService.openSnackBarSuccess('Deletion of wolf was successful');
    }, () => {
      this.wolves.splice(clickedWolfIndex, 0, clickedWolf);
      this.snackbarService.openSnackBarError('Deletion of wolf was not successful');
    });
  }

}
