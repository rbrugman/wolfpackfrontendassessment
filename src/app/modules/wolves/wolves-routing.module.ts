import { WolvesAddEditComponent } from './components/wolves-add-edit/wolves-add-edit.component';
import { WolvesOverviewComponent } from './components/wolves-overview/wolves-overview.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'overview' },
  { path: 'overview', component: WolvesOverviewComponent },
  { path: 'add', component: WolvesAddEditComponent },
  { path: 'edit/:id', component: WolvesAddEditComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WolvesRoutingModule { }
