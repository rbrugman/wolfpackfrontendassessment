import { SharedModule } from './../shared/shared.module';
import { ClarityModule } from '@clr/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WolvesRoutingModule } from './wolves-routing.module';
import { WolvesOverviewComponent } from './components/wolves-overview/wolves-overview.component';
import { WolvesTableComponent } from './components/wolves-table/wolves-table.component';
import { WolvesAddEditComponent } from './components/wolves-add-edit/wolves-add-edit.component';

@NgModule({
  declarations: [
    WolvesOverviewComponent,
    WolvesTableComponent,
    WolvesAddEditComponent],
  imports: [
    CommonModule,
    WolvesRoutingModule,
    SharedModule
  ]
})
export class WolvesModule { }
