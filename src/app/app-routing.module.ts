import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'wolves', pathMatch: 'full' },
  { path: 'wolves', loadChildren: () => import('./modules/wolves/wolves.module').then(m => m.WolvesModule) },
  { path: 'packs', loadChildren: () => import('./modules/packs/packs.module').then(m => m.PacksModule) }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
