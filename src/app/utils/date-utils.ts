/**
 * Utility class to format dates and date strings.
 */
export class DateUtils {

    /**
     * Method to format a date to a YYYY-MM-DD format.
     * @param date The date to format.
     */
    public static formatDateToDateString(date: Date): string {
        let month = '' + (date.getMonth() + 1);
        let day = '' + date.getDate();
        const year = date.getFullYear();

        if (month.length < 2) {
            month = '0' + month;
        }
        if (day.length < 2) {
            day = '0' + day;
        }
        return [year, month, day].join('-');
    }

    /**
     * Method to reverse the format of a date string with one of the following formats (DD-MM-YYYY) or (YYYY-MM-DD).
     * @param date the date string to reverse.
     */
    public static reverseDateString(date: string): string {
        const dateStringParts = date.split('-');
        if (dateStringParts == null || dateStringParts.length !== 3) {
            return '';
        }
        return `${dateStringParts[2]}-${dateStringParts[1]}-${dateStringParts[0]}`;
    }
}
