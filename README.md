# Wolfpack Frontend Assessment
A application to manage wolves & packs. 

## Table of Contents
* [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)

## Built With
* [Angular](https://angular.io/docs)
* [Clarity Design](https://clarity.design/)
* [Angular Material](https://material.angular.io/)
* [Leaflet](https://github.com/Asymmetrik/ngx-leaflet)


## Getting Started

### Prerequisites
This project requires:
* [Node.js](https://nodejs.org/about/releases/) to be installed.
* (optional) npm to be installed (gets automatically installed with Node.js)
```sh
npm install npm@latest -g
```
* Angular CLI
```sh
npm install -g @angular/cli
```

You can check if npm and the Angular CLI are correctly installed with the following commands:
```sh
npm version
ng v
```

### Installation
1. Clone the repo
2. Install NPM packages
```sh
npm install
```
3. Open repo folder with VS Code
4. Open new terminal `CTRL + SHIFT + ~`
4. Serve the application with the Angular CLI
```sh
ng s -o
```